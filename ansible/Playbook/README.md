# Howto:

Start controller:

```
ansible-playbook -i inventory morf_controller_start.yml
```

Stop controller:

```
ansible-playbook -i inventory morf_controller_stop.yml
```

Transfer controller to robot:

```
ansible-playbook -i inventory morf_transfer_controller.yml
```

Transfer dynamixel workspaces to robot:
```
ansible-playbook -i inventory  morf_update_dynamixel.yml
```


To see raw log files on robot:
```
ssh morf-one@morf-onepc.local
```

Cd to Service you want to see:

```
cd ros-dynamixel-driver
```

Run tail command to follow file:
```
tail -f log/main/current
```

Note: If noting is  suddenly not written to the file(current), the file might have rotated. Stop the tail command, and start it again to follow the new file.
